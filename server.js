const express = require('express')

const app = express()
app.use(express.json())

const carRouter = require('./routes/car')

app.use(carRouter)

app.listen(4000, '0.0.0.0', () => {
    console.log('server started on port:4000')
})