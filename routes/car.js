const express = require('express')
const db = require('../db')

const router = express.Router()

router.get('/allcars', (request,response) => {
    const statement = `SELECT * FROM car`
    db.pool.query(statement, (error,data) => {
        if(error) {
            response.send({status:'error',error})
        } else if(!data || data.length === 0) {
            response.send({status:'error',error:'cars not found'})
        } else {
            response.send({status:'success',data})
        }
    })
})

router.get('/car/:id', (request,response) => {
    const id = request.params.id
    const statement = `SELECT car_name, company_name, car_prize FROM car WHERE car_id = ?`
    db.pool.query(statement,[id], (error,data) => {
        if(error) {
            response.send({status:'error',error})
        } else if(!data || data.length === 0) {
            response.send({status:'error',error:'cars not found'})
        } else {
            response.send({status:'success',data})
        }
    })
})

router.post('/car', (request,response) => {
    const {name, company, price} = request.body
    const statement = `INSERT INTO car (car_name, company_name, car_prize) VALUES (?,?,?)`
    db.pool.query(statement,[name, company, price], (error,data) => {
        if(error) {
            response.send({status:'error',error})
        } else {
            response.send({status:'success',data})
        }
    })
})

router.put('/car/:id', (request,response) => {
    const id = request.params.id
    const {name, company, price} = request.body
    const statement = `UPDATE car SET car_name =?, company_name =?, car_prize =? WHERE car_id =?`
    db.pool.query(statement,[name, company, price, id], (error,data) => {
        if(error) {
            response.send({status:'error',error})
        } else {
            response.send({status:'success',data})
        }
    })
})

router.delete('/car/:id', (request,response) => {
    const id = request.params.id
    const statement = `DELETE FROM car WHERE car_id =?`
    db.pool.query(statement,[id], (error,data) => {
        if(error) {
            response.send({status:'error',error})
        } else {
            response.send({status:'success',data})
        }
    })
})

module.exports = router