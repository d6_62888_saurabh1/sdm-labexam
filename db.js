const mysql = require('mysql')

const pool = mysql.createPool({
    host:'localhost',
    user:'root',
    password:'password',
    database:'car_db',
    port:3306,
    connectionLimit:10
})

module.exports = {
    pool,
}